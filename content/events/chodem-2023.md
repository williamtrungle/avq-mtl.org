---
title: >
    **Chợ Đêm** &mdash; Notre 1ère a eu lieu au Bassin Peel en juin 2023
video: chodem-2023.mp4
link: "https://chodem-mtl.com"
date: 2023-06-15T17:00:00-05:00
---
Chợ Đêm est une célébration de l'identité culturelle vietnamienne au Québec
sous la forme d'un grand marché *street food*. Cet événement rassembleur
mettant en valeur la cuisine vietnamienne ainsi que les marchands et activités
culturelles a été assisté par plus de 15 000 personnes lors notre première
édition!
