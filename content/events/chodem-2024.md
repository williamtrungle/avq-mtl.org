---
title: >
    **Chợ Đêm** &mdash; Deuxième édition en juillet 2024
video: chodem-2024.mp4
link: "https://chodem-mtl.com"
date: 2024-06-15T17:00:00-05:00
---

La 2e édition de Chợ Đêm au Bassin Peel a été un grand success avec 25 000
festivaliers, plus de 40 performances artistiques et plus de 40 kiosques!
