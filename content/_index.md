---
title: "Association Vietnamiens Qu&eacute;b&eacute;cois"
members:
    - name: Trang_Tran
      title: Présidente
      instagram: trangreeny
    - name: Charles_Nguyen
      title: Vice-président
      instagram: mtlcharles
    - name: William_T._Le
      title: Secrétaire
      instagram: willtle
    - name: Michel_Nguyen
      title: Trésorier
      instagram: michiiimich
socials:
    - name: email
      url: mailto:chodem.mtl@gmail.com
      icon: email.png
    - name: facebook
      url: https://www.facebook.com/avq.mtl/
      icon: facebook.png
    - name: instagram
      url: https://instagram.com/chodemmtl/
      icon: instagram.png
---
Un organisme à but non-lucratif dont sa première mission est d'inspirer et
d'unir la vibrante communauté des jeunes Vietnamiens, Québécois et Canadiens de
toute origine à Montréal à travers une profonde appréciation et célébration de
la culture vietnamienne. Nous visons à créer un sentiment d'appartenance et
à favoriser les échanges culturels pour les Vietnamiens de tout âge.
